package in.silentsudo.plainws.plainwebsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlainWebsocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlainWebsocketApplication.class, args);
    }

}
