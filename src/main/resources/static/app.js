var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

var token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqb2huIiwiaXNzIjoiYXBwLXNlcnZlciIsImF1ZCI6Im1vYmlsZS13ZWIiLCJqdGkiOiI2YzllZTAyMi0yNmJjLTRmMjctOWMxOC1mNWFjMzA0MDg3ZDMiLCJpYXQiOjE2NDE2NjQwMTQsImV4cCI6MTY0MTY3NDgxNH0.C-riasC5nJLUnumTHws8xvV4GI3-VaKzgye3jHXY3I6pBkPsGtLHs_PTtQ4n_Z80eAk8rfe4pcQyLZEGKgoOFw";
headers = {"Authorization": "Bearer " + token};

function connect() {
    var socket = new SockJS('http://localhost:8080/stompws');
    stompClient = Stomp.over(socket);
    stompClient.connect(headers, stompSuccess, stompFailure)
    // stompClient.connect({"Authorization": "Bearer " + token}, function (frame) {
    //     setConnected(true);
    //     console.log('Connected: ' + frame);
    //     stompClient.subscribe('/topic/greetings', function (greeting) {
    //         showGreeting(JSON.parse(greeting.body).text);
    //     });
    // });
}

function stompSuccess(frame) {
    setConnected(true)
            stompClient.subscribe('/topic/greetings/', function (greeting) {
        showGreeting(JSON.parse(greeting.body).text);
    });
}

function stompFailure(e) {
    console.log("Failure stomp");
    // console.log(e);
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/stompws-app/newMessage", {}, JSON.stringify({'text': $("#name").val()}));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendName();
    });
});